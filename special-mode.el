(require 'transient)
(require 'derived)
(require 'log4e)
(log4e:deflogger "ytdl" "%t [%l] %m" "%H:%M:%S" '((fatal . "fatal")
                                                  (error . "error")
                                                  (warn  . "warn")
                                                  (info  . "info")
                                                  (debug . "debug")
                                                  (trace . "trace")))


(ytdl--log-enable-logging)
;; (ytdl--log-open-log)
;; (ytdl--log-clear-log)



(require 'cl-lib)

(defvar ytdl--dl-list-mode-map
  (let ((map (copy-keymap special-mode-map)))
    (prog1 map
      (define-key map "?" #'my-dispatch)
      ))
  "Keymap for `mymode'")

(transient-define-prefix my-dispatch ()
  "Invoke a Magit command from a list of available commands."
  ["ytdl-download-list commands"
   [("A" "Apply" test)
    ("g" "refresh" ytdl--refresh-download-list-buffer)
    ("k" "delete" ytdl--delete-item)]])



(define-derived-mode ytdl--dl-list-mode special-mode "ytdl-mode"
  "Major mode for `ytdl' download list."
  (hl-line-mode)
  (use-local-map ytdl--dl-list-mode-map)
  (setf truncate-lines t
        header-line-format
        (format "%-40s %-15s %s"
                "Title" "Status" "Type")))

(setq ytdl--dl-buffer-name "ytdl-dl-list")


(cl-defstruct ytdl--list-entry
  title
  status
  type
  path
  process-id)

;; TODO? Make it a hash?? with key being a UID
(setq ytdl--download-list (make-hash-table :test 'equal))

(puthash 1 (make-ytdl--list-entry :title "hello"
                                  :status "downloading"
                                  :type "music"
                                  :path nil)
         ytdl--download-list)
(puthash 2 (make-ytdl--list-entry :title "hello2"
                                  :status "downloading"
                                  :type "music"
                                  :path nil)
         ytdl--download-list)


(setq ytdl--mapping-list '())

(defun ytdl--refresh-download-list-buffer()
  (interactive)
  (message "refreshing")
  (setq ytdl--mapping-list nil)
  (pop-to-buffer (with-current-buffer (get-buffer-create ytdl--dl-buffer-name)
                   (let ((inhibit-read-only t))
                     (erase-buffer)
                     (maphash (lambda (key item)
                                (setq ytdl--mapping-list (append ytdl--mapping-list `(,key)))
                                (insert (format "%-60s %-15s %s\n"
                                                (ytdl--list-entry-title item)
                                                (ytdl--list-entry-status item)
                                                (ytdl--list-entry-type item))))
                              ytdl--download-list)
                     (ytdl--dl-list-mode)
                     (current-buffer)))))

(defun test ()
  (interactive)
  (message "test"))

(defun  ytdl--delete-item ()
  (interactive)
  (let ((index (1- (line-number-at-pos))))
    (ytdl--info (concat "Removing index "
                        (int-to-string index)))
    (remhash (nth index ytdl--mapping-list) ytdl--download-list)
    (ytdl--refresh-download-list-buffer)))




(ytdl--refresh-download-list-buffer)
;; (ytdl--log-open-log)

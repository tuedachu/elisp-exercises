;;; -*- lexical-binding: t -*-

(setq test 0)

;; that does not work...
(async-start
 (lambda()
   (sleep-for 1)
   (+ 1 test))
 (lambda (result)
   (message "result %s" result)))


;; But creating a function seems to work..
(defun my-async-function (test)
  (async-start
   (lambda()
     (sleep-for 10)
     (+ 1 test))
   (lambda (result)
     (message "result %s" result)
     (message (int-to-string test)))))

(my-async-function test)
(setq test 10)

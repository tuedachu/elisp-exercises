(setq my-alist '((pine . cones)
                 ("oak" . acorns)
                 (maple . seeds)))

(cdr (assoc "oak" my-alist))

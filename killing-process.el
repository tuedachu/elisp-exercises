(setq p (async-start
         (lambda()
           (sleep-for 5)
           (call-process "touch" nil nil nil (expand-file-name "~/temp/t")))
         (lambda (result)
           (message "finished"))))

p
(interrupt-process p)
